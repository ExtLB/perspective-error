Ext.define('Client.view.perspective.error.View', {
	extend: 'Ext.Container',
	xtype: 'perspective.error',
	controller: {type: 'perspective.error'},
	viewModel: {type: 'perspective.error'},
  requires: [
    'Ext.layout.Center',
    'Ext.util.History',
    'Ext.Button',
    'Ext.Label'
  ],
	layout: 'center',
  cls: 'perspective-error',
  userCls: 'transparent-background',
  listeners: {
	  initialize: 'onErrorViewInitialize'
  },
  items: [{
    xtype: 'toolbar',
    docked: 'top',
    items: [{
      xtype: 'button',
      ui: 'rounded raised',
      handler: () => {
        Ext.util.History.back();
      },
      bind: {
        text: '{"per-error:BACK":translate}'
      }
    }, {
      xtype: 'spacer'
    }, {
      xtype: 'label',
      bind: {
        hidden: '{hideRequestId}',
        html: '{"per-error:REQUEST_ID":translate}: {requestId}'
      }
    }]
  }, {
	  xtype: 'container',
    cls: 'perspective-error-inner-container',
    width: 600,
    layout: {
      type: 'vbox',
      align: 'center',
      pack: 'center'
    },
    items: [{
      xtype: 'label',
      bind: {
        html: '{errorCode}',
      },
      cls: 'perspective-error-top-text',
    }, {
      xtype: 'label',
      cls: 'perspective-error-desc',
      bind: {
        html: '{errorCodeMessage}',
      }
    }]
  }]
});
