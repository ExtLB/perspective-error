Ext.define('Client.view.perspective.error.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.perspective.error',

	routes: {
		'error(/:{errorCode})(/:{requestId})': {action: 'errorRoute'}
	},
	errorRoute:function(params) {
    let me = this;
    let view = me.getView();
    let vm = me.getViewModel();
    vm.set('errorCode', params.errorCode);
    vm.set('errorCodeMessage', i18next.t(`spe:ERROR.${params.errorCode}`));
    if (!_.isUndefined(params.requestId)) {
      vm.set('requestId', params.requestId);
    } else {
      vm.set('requestId', null);
    }
    // let errorCode = params.errorCode;
    // try {
	   //  if (_.isUndefined(errorCode)) { return false; }
	   //  console.log('Error Route Executing');
    //
    //   let exists = Ext.ClassManager.getByAlias('widget.perspective.error.' + errorCode);
    //   if (exists === undefined) {
    //     console.log(errorCode + ' does not exist');
    //     return;
    //   }
    //   if (!view.getComponent(errorCode)) {
    //     view.removeAll(true, true);
    //     view.add({xtype: `perspective.error.${errorCode}`, itemId: errorCode});
    //   }
    //   view.setActiveItem(errorCode);
    //   // vm.set('heading', node.get('text'));
    // } catch (err) {
	   //  console.error(err);
    // }
	},

  onErrorViewInitialize: function () {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    // setTimeout(() => {
    //   vm.getStore('desktopmenu').load();
    // }, 1);
  }

//	onActionsViewLogoutTap: function( ) {
//		var vm = this.getViewModel();
//		vm.set('firstname', '');
//		vm.set('lastname', '');
//
//		Session.logout(this.getView());
//		this.redirectTo(AppCamp.getApplication().getDefaultToken().toString(), true);
//	}

});
